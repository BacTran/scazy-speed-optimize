﻿using UnityEngine;
using System.Collections;

public class ShopManager : MonoBehaviour {

    public static ShopManager instance;

    public int indexActiveChara;
    public int indexActiveEnvi;
    public int coinMain;
    EnviManager enviManager;
    CharacterManager characterManager;

    string oldNameEnvi;
    string oldNameCharacter;

    public int[] enviActive;
    public int[] characActive;
    // Use this for initialization
    void Start () {

        if (instance != null)
        {
            Destroy(gameObject);

        }
        else
        {
            instance = this;
            Init();
        }
	
	}

    public void Init()
    {
        indexActiveEnvi = PlayerPrefs.GetInt("ActiveEnvis", 0);
        indexActiveChara = PlayerPrefs.GetInt("ActiveCharactors", 0);

        coinMain = PlayerPrefs.GetInt("CoinMain", 5000);

        enviManager = GetComponent<EnviManager>();
        characterManager = GetComponent<CharacterManager>();

        oldNameEnvi = PlayerPrefs.GetString("Envi", "Element");
        Envi newEnvi = enviManager.GetEnviFormName(oldNameEnvi);
        newEnvi.BG.SetActive(true);
        newEnvi.BGOBJ.SetActive(true);
        newEnvi.Path.SetActive(true);

        oldNameCharacter = PlayerPrefs.GetString("Charac", "Bunny");
        Character newCharacter = characterManager.GetCharacterFormName(oldNameCharacter);
        newCharacter.character.SetActive(true);

        enviActive = new int[3];
        characActive = new int[5];

        PlayerPrefs.SetInt("Envi 0", 1);
        PlayerPrefs.SetInt("Charac 0", 1);
        PlayerPrefs.Save();

        for (int i = 0; i < enviActive.Length; i++)
        {
            string str = "Envi " + i;
            enviActive[i] = PlayerPrefs.GetInt(str, 0);
        }
        for (int i = 0; i < characActive.Length; i++)
        {
            string str = "Charac " + i;
            characActive[i] = PlayerPrefs.GetInt(str, 0);
        }
    }

	public void SetIndexCharac(int value)
    {

        indexActiveChara = value;
        PlayerPrefs.SetInt("ActiveCharactors", indexActiveChara);
        PlayerPrefs.Save();

        string str = "Charac " + value;
        characActive[value] = 1;
        PlayerPrefs.SetInt(str, characActive[value]);
        PlayerPrefs.Save();
    }
    public void SetIndexEnvi(int value)
    {
        indexActiveEnvi = value;
        PlayerPrefs.SetInt("ActiveEnvis", indexActiveEnvi);
        PlayerPrefs.Save();

        string str = "Envi " + value;
        enviActive[value] = 1;
        PlayerPrefs.SetInt(str, enviActive[value]);
        PlayerPrefs.Save();
    }
    public void SetNameEnvi(string name)
    {
        Envi oldEnvi = enviManager.GetEnviFormName(oldNameEnvi);
        oldEnvi.BG.SetActive(false);
        oldEnvi.BGOBJ.SetActive(false);
        oldEnvi.Path.SetActive(false);

        oldNameEnvi = name;
        PlayerPrefs.SetString("Envi", oldNameEnvi);
        PlayerPrefs.Save();

        Envi newEnvi = enviManager.GetEnviFormName(oldNameEnvi);
        newEnvi.BG.SetActive(true);
        newEnvi.BGOBJ.SetActive(true);
        newEnvi.Path.SetActive(true);

    }

    public void SetNameCharacter(string name)
    {
        Character oldCharacter = characterManager.GetCharacterFormName(oldNameCharacter);
        oldCharacter.character.SetActive(false);

        oldNameCharacter = name;
        PlayerPrefs.SetString("Charac", oldNameCharacter);
        PlayerPrefs.Save();

        Character newCharacter = characterManager.GetCharacterFormName(oldNameCharacter);
        newCharacter.character.SetActive(true);

    }
}
