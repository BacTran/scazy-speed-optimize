﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScrollRectSnap : MonoBehaviour {

    public Text coinStoreUI;
    public Text nameItemStoreUI;
    public GameObject btnStoreUI;
    public RectTransform pancel;
    public ShopItem[] imgGroup;
    public RectTransform center;

    float[] distance;
    bool dragging = false;
    int imgDistance;
    int minImageNum;

    void Start()
    {
        int imgLenght = imgGroup.Length;
        distance = new float[imgLenght];

        imgDistance = (int)Mathf.Abs(imgGroup[1].img.GetComponent<RectTransform>().anchoredPosition.x - imgGroup[0].img.GetComponent<RectTransform>().anchoredPosition.x);
        
        StartCoroutine("ScrollRectSnapImg");




        imgGroup[ShopManager.instance.indexActiveEnvi].img.GetComponent<RectTransform>().localScale = Vector3.one;

    }
    void Update()
    {
    }

    IEnumerator aniSnapImgActive()
    {
        float newX = Mathf.Lerp(pancel.anchoredPosition.x, ShopManager.instance.indexActiveEnvi*(-imgDistance), 1);
        Vector2 newPosition = new Vector2(newX, pancel.anchoredPosition.y);
        pancel.anchoredPosition = newPosition;
        yield return null;
    }

    IEnumerator ScrollRectSnapImg()
    {
        yield return StartCoroutine("aniSnapImgActive");
        while (true)
        {
            for (int i = 0; i < imgGroup.Length; i++)
            {
                distance[i] = (int)Mathf.Abs(center.transform.position.x - imgGroup[i].img.transform.position.x);
            }

            float minDistance = Mathf.Min(distance);
            
            for(int i= 0; i < imgGroup.Length; i++)
            {
                if(distance[i] < 369)
                {
                    if (distance[i] > 0)
                    {
                        Double perLocalScale = Math.Round((1 - distance[i] / 369),6);
                        if (perLocalScale >.7f)
                        {
                            imgGroup[i].img.GetComponent<RectTransform>().localScale = Vector3.one * (float)perLocalScale;
                        }

                    }
                }
                else
                {
                    imgGroup[i].img.GetComponent<RectTransform>().localScale = Vector3.one * .7f;
                }
            }

            //percent += Time.deltaTime * attackSpeed;
            //float interpolation = (-Mathf.Pow(percent, 2) + percent) * 4;
            //transform.position = Vector3.Lerp(originalPosition, attackPosition, interpolation);

            for (int i = 0; i < imgGroup.Length; i++)
            {
                if (minDistance == distance[i])
                {
                    minImageNum = i;
                    if (ShopManager.instance.enviActive[minImageNum] == 0)
                    {
                        
                        btnStoreUI.SetActive(true);
                        coinStoreUI.text = imgGroup[i].coin.ToString();
                    }
                    else
                    {
                        ShopManager.instance.SetIndexEnvi(minImageNum);
                        ShopManager.instance.SetNameEnvi(imgGroup[minImageNum].id);
                        btnStoreUI.SetActive(false);
                    }

                    nameItemStoreUI.text = imgGroup[i].id;
                }
            }

            if (!dragging)
            {
                LerpToImg(minImageNum * -imgDistance);
            }
            yield return null;
        }
    }

    private void LerpToImg(int pos)
    {
        float newX = Mathf.Lerp(pancel.anchoredPosition.x, pos, Time.deltaTime );
        Vector2 newPosition = new Vector2(newX, pancel.anchoredPosition.y);
        pancel.anchoredPosition = newPosition;
        

    }

    public void StartDrag()
    {
        dragging = true;
    }
    public void EndDrag()
    {
        dragging = false;
    }

    public void btn_Store()
    {
        if (ShopManager.instance.coinMain > imgGroup[minImageNum].coin)
        {
            ShopManager.instance.coinMain -= imgGroup[minImageNum].coin;
            PlayerPrefs.SetInt("CoinMain", ShopManager.instance.coinMain);
            PlayerPrefs.Save();

            imgGroup[minImageNum].coin = 0;
            ShopManager.instance.SetIndexEnvi(minImageNum);
            ShopManager.instance.SetNameEnvi(imgGroup[minImageNum].id);
        }
    }

   

    void OnEnable()
    {
        imgGroup[ShopManager.instance.indexActiveEnvi].img.GetComponent<RectTransform>().localScale = Vector3.one;
        StartCoroutine("ScrollRectSnapImg");
    }
    void OnDisable()
    {
        StopCoroutine("ScrollRectSnapImg");
        float newX = Mathf.Lerp(pancel.anchoredPosition.x, ShopManager.instance.indexActiveChara * (-imgDistance), 1);
        Vector2 newPosition = new Vector2(newX, pancel.anchoredPosition.y);
        pancel.anchoredPosition = newPosition;
        btnStoreUI.SetActive(false);
    }



    [Serializable]
    public class ShopItem
    {
        public string id;
        public Image img;
        public int coin;

    }
}
