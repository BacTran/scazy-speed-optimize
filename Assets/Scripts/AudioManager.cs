﻿using UnityEngine;
using System.Collections;

public class AudioManager : MonoBehaviour {

    public static AudioManager intance;
    public float masterVolumePercent { get; private set; }
    AudioSource[] musicSource;
    int activeMusicSourceIndex;

    SoundLibrary library;
    public bool isVolum;
    public bool isPlay;
    void Start () {
        if (intance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            intance = this;
            DontDestroyOnLoad(gameObject);

            library = GetComponent<SoundLibrary>();
            musicSource = new AudioSource[2];
            for (int i = 0; i < 2; i++)
            {
                GameObject newMusicSource = new GameObject(" Music Source " + (i + 1));
                musicSource[i] = newMusicSource.AddComponent<AudioSource>();
                newMusicSource.transform.parent = transform;
            }
            masterVolumePercent = PlayerPrefs.GetFloat("master vol", 1);
        }
    }
	
	

    public void PlayMusic(AudioClip clip, float fadeDuration = 1)
    {
        activeMusicSourceIndex = 1 - activeMusicSourceIndex;
        musicSource[activeMusicSourceIndex].clip = clip;
        musicSource[activeMusicSourceIndex].Play();

        StartCoroutine(AnimateMusicCrossfade(fadeDuration));
    }

    public void PlaySound(AudioClip clip, Vector3 pos)
    {
        if (clip != null)
        {
            AudioSource.PlayClipAtPoint(clip, pos, masterVolumePercent);
        }
    }
    public void PlaySound(string soundName, Vector3 pos)
    {
        PlaySound(library.GetClipFormName(soundName), pos);
    }

    IEnumerator AnimateMusicCrossfade(float duration)
    {
        float percent = 0;

        while (percent < 1)
        {
            percent += Time.deltaTime * 1 / duration;
            musicSource[activeMusicSourceIndex].volume = Mathf.Lerp(0, masterVolumePercent, percent);
            musicSource[1 - activeMusicSourceIndex].volume = Mathf.Lerp(masterVolumePercent, 0, percent);
            yield return null;
        }

        musicSource[1 - activeMusicSourceIndex].clip = null;

    }
    public void OnVolum(float volum)
    {
        masterVolumePercent = volum;
        musicSource[0].volume = masterVolumePercent;
        musicSource[1].volume = masterVolumePercent;

        PlayerPrefs.SetFloat("master vol", masterVolumePercent);
    }
}
