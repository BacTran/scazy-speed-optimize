﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class PlayerController : ILiving
{

    Rigidbody2D myRigidbody2D;
    Animator myAnimator;


    int currentJump = 0;

    public static event System.Action OnTriggerStaticGold;

    [HideInInspector]
    public bool isJump;
    [HideInInspector]
    public bool isFly;
    [HideInInspector]
    public bool isGiant;

    [HideInInspector]
    public bool isAttack = false;
    public static PlayerController instace;

    public bool isState;

    IEnumerator aniItem;

    public override void Start()
    {
        base.Start();

        instace = this;

        myRigidbody2D = GetComponent<Rigidbody2D>();
        myAnimator = GetComponent<Animator>();
    }

    public void Jump(float jumpVelocity)
    {
        if (!isJump)
        {
            if (currentJump < 2 && !SpeedManager.instance.Jump)
            {
                
                myRigidbody2D.velocity = new Vector2(0, jumpVelocity);
                //myRigidbody2D.AddForce(new Vector2(0, jumpVelocity), ForceMode2D.Impulse);
                currentJump++;
                myAnimator.SetBool("isGround", false);
            }
        }

    }

    void OnCollisionStay2D(Collision2D collision2d)
    {
        myAnimator.SetBool("isGround", true);
        currentJump = 0;

    }

    public void TakeItem(Collider2D collider2d)
    {
        if (!isFly)
        {
            if (collider2d.tag.CompareTo("Broom") == 0)
            {

                SpeedManager.instance.thayDoitocDokhiAnBroom(4);
                SetAniGiant(false);
                SetAniShield(false);
                myAnimator.SetBool("isFly", true);

                isFly = true;
                isJump = true;

                Destroy(collider2d.gameObject);

            }
            else {

                if (collider2d.tag.CompareTo("Decrease Speed Lotion") == 0)
                {

                    SpeedManager.instance.GiamToc();

                    Destroy(collider2d.gameObject);
                }
                else
                {
                    if (collider2d.tag.CompareTo("Giant Lotion") == 0)
                    {
                        SpeedManager.instance.thayDoitocDokhiAnGiant(3);
                        isAttack = true;


                        myAnimator.SetBool("isGiant", true);
                        isJump = true;
                        Destroy(collider2d.gameObject);
                    }
                    else
                    {
                        if (collider2d.tag.CompareTo("Shield") == 0)
                        {
                            isAttack = true;
                            SpeedManager.instance.thayDoitocDokhiAnShield(3);


                            myAnimator.SetBool("isShield", true);

                            isJump = true;
                            Destroy(collider2d.gameObject);
                        }
                        else
                        {
                            if (collider2d.tag.CompareTo("Coin") == 0)
                            {
                                AudioManager.intance.PlaySound("Coin", collider2d.transform.position);
                                if (OnTriggerStaticGold != null)
                                {
                                    OnTriggerStaticGold();
                                }
                                Destroy(collider2d.gameObject);
                            }
                        }

                    }
                }

            }
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {

        if (isAttack)
        {

            IDmageable DmageTrap = other.GetComponent<IDmageable>();
            if (DmageTrap != null)
            {
                DmageTrap.TakeDamage(1);
            }

        }

    }

    public override void Die()
    {
        base.Die();
        myAnimator.SetBool("isAlive", false);

        SpeedManager.instance.playisdead = true;
    }

    public void SetAniGiant(bool active)
    {
        myAnimator.SetBool("isGiant", active);
        myAnimator.SetBool("isShield", false);
        myAnimator.SetBool("isFly", false);
        isJump = false;
        isAttack = false;
    }

    public void SetAniShield(bool active)
    {
        myAnimator.SetBool("isShield", active);
        myAnimator.SetBool("isGiant", false);
        myAnimator.SetBool("isFly", false);
        isJump = false;
        isAttack = false;
    }

    public void SetAniFly(bool active)
    {
        myAnimator.SetBool("isFly", active);
        myAnimator.SetBool("isGiant", false);
        myAnimator.SetBool("isShield", false);
        isJump = false;
        isFly = false;
    }
}

