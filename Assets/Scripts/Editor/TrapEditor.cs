﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(TrapsManager))]
public class TrapEditor : Editor
{

    public override void OnInspectorGUI()
    {
        TrapsManager traps = target as TrapsManager;
        if (DrawDefaultInspector())
        {
        }

        if (GUILayout.Button("Create Title"))
        {
            traps.CreateTitle();
        }

        if (GUILayout.Button("Grenerate Trap")) // đoạn tạo button sinh trap
        {
            traps.GenerateTrap();
        }
    }
}
