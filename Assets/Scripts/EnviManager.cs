﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnviManager : MonoBehaviour {

    public Envi[] EnviGroups;

   Dictionary<string, Envi> groupDictionary = new Dictionary<string, Envi>();

    void Awake()
    {
        foreach (Envi envi in EnviGroups)
        {
            groupDictionary.Add(envi.groupID, envi);
        }
    }

    public Envi GetEnviFormName(string name)
    {
        if (groupDictionary.ContainsKey(name))
        {
            Envi envi = groupDictionary[name];
            return envi;
        }
        return null;
    }


    
}
[System.Serializable]
public class Envi
{
    public string groupID;
    public GameObject BG;
    public GameObject BGOBJ;
    public GameObject Path;
}
