﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CharacterManager : MonoBehaviour
{

    public Character[] CharacterGroups;

    Dictionary<string, Character> groupDictionary = new Dictionary<string, Character>();

    void Awake()
    {
        foreach (Character charac in CharacterGroups)
        {
            groupDictionary.Add(charac.groupID, charac);
        }
    }

    public Character GetCharacterFormName(string name)
    {
        if (groupDictionary.ContainsKey(name))
        {
            Character character = groupDictionary[name];
            return character;
        }
        return null;
    }



}
[System.Serializable]
public class Character
{
    public string groupID;
    public GameObject character;
}
