﻿using UnityEngine;
using System.Collections;
using System;

public class ILiving : MonoBehaviour,IDmageable {

    public float startingHeath;
    public float heath { get; protected set; }
    public bool dead;
    public event System.Action OnDeath;

    int oldTime;
    public virtual void Start()
    {
        SpeedManager.instance.OnDamge += Die;
        heath = startingHeath;
    }

    public void TakeHit(float pecSpeed, float timeExits, Collider2D collider)
    {

        SpeedManager.instance.thayDoitocDOkhiVaTrap(timeExits, pecSpeed);
    }

    public virtual void Die()
    {
        dead = true;
        if (OnDeath != null)
        {
            OnDeath();
        }
    }

    public void TakeDamage(float damage)
    {
        heath -= damage;
        if (heath < 0)
        {
            Die();
        }
    }
}
