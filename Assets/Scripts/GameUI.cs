﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameUI : MonoBehaviour {

    

    public GameObject gameoverUI;
    public GameObject pauseUI;
    public GameObject btnPause;
    public GameObject mainPlayUI;
    public GameObject shopUI;
    public GameObject shopEnviUI;
    public GameObject shopCharacterUI;
    public GameObject btn_envi;
    public GameObject btn_chara;

    public Text scoreUI;
    public Text goldUI;
    public Text coinMain;
    public Text gameoverScoreUI;
    public Text gameoverBestUI;
    public Text gameoverCoinUI;

    public RectTransform heathBar;

    int hightScore;
    PlayerController player;
    int coin;
    // Use this for initialization
    void Start () {
        player = FindObjectOfType<PlayerController>();
        player.OnDeath += GameOver;
        hightScore = PlayerPrefs.GetInt("HightScore", 0);
        coin = PlayerPrefs.GetInt("CoinMain", 0);
    }
	
	// Update is called once per frame
	void Update () {

        coinMain.text = ShopManager.instance.coinMain.ToString();

        float heathPercent = 0;
        if (player != null)
        {
            if(SpeedManager.instance.indexGuage >-1)
                heathPercent = SpeedManager.instance.indexGuage / player.startingHeath;
        }
        heathBar.localScale = new Vector3(heathPercent, 1, 1);

        if (ScoreKeeper.score >= 0)
        {
            scoreUI.text = ScoreKeeper.score.ToString();
        }
        else
        {
            scoreUI.text = "0";
        }
        
        goldUI.text = ScoreKeeper.gold.ToString();
	}

    public void BtnOnPauseUI()
    {
        btnPause.SetActive(false);
        pauseUI.SetActive(true);
        SpeedManager.instance.Pause = true;

    }
    public void OnTriggerEnter_BtnPause()
    {
        SpeedManager.instance.Jump = true;
    }
    public void BtnDisPauseUI()
    {
        SpeedManager.instance.Pause = false;
        SpeedManager.instance.Jump = false;
        pauseUI.SetActive(false);
        btnPause.SetActive(true);

    }
    public void BtnRePlayUI()
    {
        ScoreKeeper.score = 0;
        ScoreKeeper.gold = 0;
        SpeedManager.instance.speed = 1;
        SpeedManager.instance.Jump = false;
        SceneManager.LoadScene("CrazySpeed");
    }
    public void BtnHomeUI()
    {
        SceneManager.LoadScene("Menu");
        SpeedManager.instance.Pause = false;
        AudioManager.intance.isPlay = false;
        SpeedManager.instance.Jump = false;
        SpeedManager.instance.speed = 1;

    }
    public void GameOver()
    {
        AudioManager.intance.PlaySound("GameOver", transform.position);
        SpeedManager.instance.Jump = true;
        mainPlayUI.SetActive(false);
        Cursor.visible = true;
        gameoverScoreUI.text = scoreUI.text;
        gameoverCoinUI.text = goldUI.text;
        if (ScoreKeeper.score > hightScore)
        {
            hightScore = ScoreKeeper.score;
            PlayerPrefs.SetInt("HightScore", hightScore);
        }
        gameoverBestUI.text = hightScore.ToString();
        gameoverUI.SetActive(true);

        ShopManager.instance.coinMain += ScoreKeeper.gold;
        PlayerPrefs.SetInt("CoinMain", ShopManager.instance.coinMain);
        PlayerPrefs.Save();
    }


    public void ShopCharactor()
    {
        gameoverUI.SetActive(false);
        shopUI.SetActive(true);
        shopCharacterUI.SetActive(true);
    }

    public void Btn_Charac()
    {
        btn_envi.SetActive(false);
        btn_chara.SetActive(true);
    }
    public void Btn_Envi()
    {
        btn_chara.SetActive(false);
        btn_envi.SetActive(true);
        
    }
}


