﻿using UnityEngine;
using System.Collections;

public class Gold : MonoBehaviour {

    Camera isCameraRender;
    float direction = -1;
    float speed;

    Animator myAnimator;
    SpriteRenderer mySpriteRenderer;
    SpeedManager speedManager;

    
    void Start () {

        myAnimator = GetComponent<Animator>();

        mySpriteRenderer = GetComponent<SpriteRenderer>();
        isCameraRender = GameObject.FindGameObjectWithTag("CameraRenderer").GetComponent<Camera>();
        speedManager = FindObjectOfType<SpeedManager>();

        speedManager.OnChangeSpeed += OnChangeSpeed;
        speed = speedManager.GetSpeed();


    }
	
	void Update () {
        // Movement
        if (!PlayerController.instace.dead && !SpeedManager.instance.Pause && AudioManager.intance.isPlay)
        {


            Vector3 movement = new Vector3(speed * direction, 0, 0);

            movement *= Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x + movement.x, transform.localPosition.y, transform.position.z);
        }

        //Play animator
        if (RendererExtensions.IsVisibleFrom(mySpriteRenderer, isCameraRender) == true)
        {
            myAnimator.enabled = true;
        }

        //Destroy gameobject
        if (transform.localPosition.x < 0 && mySpriteRenderer.IsVisibleFrom(isCameraRender) == false)
        {
            Destroy(gameObject);
        }
    }

    public void OnChangeSpeed()
    {
        speed = speedManager.GetSpeed();
    }

}
