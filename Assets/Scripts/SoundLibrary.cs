﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundLibrary : MonoBehaviour
{

    public SoundGroup[] soundGroups;

    Dictionary<string, AudioClip> groupDictionary = new Dictionary<string, AudioClip>();

    void Awake()
    {
        foreach (SoundGroup clip in soundGroups)
        {
            groupDictionary.Add(clip.groupID, clip.clip);
        }
    }

    public AudioClip GetClipFormName(string name)
    {
        if (groupDictionary.ContainsKey(name))
        {
            AudioClip sounds = groupDictionary[name];
            return sounds;
        }
        return null;
    }


    [System.Serializable]
    public class SoundGroup
    {
        public string groupID;
        public AudioClip clip;
    }
}
