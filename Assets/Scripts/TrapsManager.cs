﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TrapsManager : MonoBehaviour {

    string titleName= "Generate Title";
    string trapName = "Generate Trap";
    string goldName = "Generate Gold";
    
    Transform trapHolder;
    Transform goldHolder;
    Transform itemHolder;

    public Transform titlePrefab;
    public Transform[] trapPrefab;
    public Transform goldPrefab;
    public Transform[] itemPrefab;

    public int currentTitle;
    public Vector3 posTitleStart;

    int seed;
    public int minDisBetweenToTrap = 5;
    public int maxDisBetweenToTrap = 20;

    public int trapCount = 5;

    List<Transform> postitleHolder;
    List<Coord> allTileCoords;
    Queue<Coord> shuffledTileTraps;


    Transform posTrapCuoiCung;
    Transform posTileDauTien;

    

    void Start()
    {
        System.Random rdseed = new System.Random();
        seed = rdseed.Next(45861);
        postitleHolder = new List<Transform>();

        if (transform.FindChild(titleName))
        {
            for (int i = 0; i < transform.FindChild(titleName).childCount; i++)
            {
                Transform child = transform.FindChild(titleName).GetChild(i);
                postitleHolder.Add(child);
            }
        }
        posTileDauTien = postitleHolder[0];
        if (!transform.FindChild(trapName))
        {
            trapHolder = new GameObject(trapName).transform;
            trapHolder.parent = transform;
        }
        else
        {
            trapHolder = transform.FindChild(trapName).transform;
        }

        if (!transform.FindChild(goldName))
        {
            goldHolder = new GameObject(goldName).transform;
            goldHolder.parent = transform;
        }
        else
        {
            goldHolder = transform.FindChild(goldName).transform;
        }
        itemHolder = transform.FindChild("Generate Item").transform;
        
        GenerateTrap();
        for(int i = 0; i < 10; i+=7)
        {
            Vector3 trapPosition = postitleHolder[i].position;
            Transform newTrap = Instantiate(trapPrefab[GetPrefabTrap()], trapPosition, Quaternion.identity) as Transform;
            newTrap.parent = trapHolder;
        }
        

    }

    void Update()
    {
        if (posTrapCuoiCung.localPosition.x < posTileDauTien.localPosition.x)
        {
            GenerateTrap();
        }
    }

    
    public void GenerateTrap()
    {
        // code sinh

        allTileCoords = new List<Coord>();
        for (int x = 0; x < currentTitle; x++)
        {
            allTileCoords.Add(new Coord(x));
        }
        bool[] allTile = new bool[currentTitle];

        for(int i = 0; i < currentTitle; i++)
        {
            allTile[i] = false;
        }
        shuffledTileTraps = new Queue<Coord>(Utility.ShuffleArray(allTileCoords.ToArray(), ref seed, minDisBetweenToTrap, maxDisBetweenToTrap));// chỗ hoán đổi mảng
        System.Random rd = new System.Random(seed);

        //Create trap
        for (int i = 0; i < trapCount; i++)
        {
            Coord randomtrap = GetRandomTrap();
            if (i != randomtrap.x)
            {
                allTile[randomtrap.x] = true;
                Vector3 trapPosition = postitleHolder[randomtrap.x].position;
                Transform newTrap = Instantiate(trapPrefab[GetPrefabTrap()], trapPosition, Quaternion.identity) as Transform;
                newTrap.parent = trapHolder;
                posTrapCuoiCung = newTrap;

                //if (Random.value > 0.8)
                //{
                //    Vector3 goldPosition = postitleHolder[randomtrap.x].position;
                //    Transform newGold = Instantiate(goldPrefab, goldPosition + Vector3.up * 2, Quaternion.identity) as Transform;
                //    newGold.parent = goldHolder;
                //}
                

            }
            
        }


        //Create Coin
        int a = 0; int b = 0;
        float percentGold = 0.5f;
        for (int i = 0; i < currentTitle; i++)
        {
            if (allTile[i])
            {
                b = a;
                a = i;
                if (a != b && (a - b) > 15 && b != 0 && Random.value > percentGold)
                {
                    int k = (a - b) / 2 + b;
                    Vector3 goldPosition = postitleHolder[k].position;
                    Transform newGold = Instantiate(goldPrefab, goldPosition /*+ Vector3.up * 1.3f */, Quaternion.identity) as Transform;
                    newGold.parent = goldHolder;
                    allTile[k] = true;

                    goldPosition = postitleHolder[k + 2].position;
                    newGold = Instantiate(goldPrefab, goldPosition, Quaternion.identity) as Transform;
                    newGold.parent = goldHolder;
                    allTile[k + 2] = true;
                    goldPosition = postitleHolder[k - 2].position;
                    newGold = Instantiate(goldPrefab, goldPosition, Quaternion.identity) as Transform;
                    newGold.parent = goldHolder;
                    allTile[k - 2] = true;

                    goldPosition = postitleHolder[k + 4].position;
                    newGold = Instantiate(goldPrefab, goldPosition, Quaternion.identity) as Transform;
                    newGold.parent = goldHolder;
                    allTile[k + 4] = true;
                    goldPosition = postitleHolder[k - 4].position;
                    newGold = Instantiate(goldPrefab, goldPosition, Quaternion.identity) as Transform;
                    newGold.parent = goldHolder;
                    allTile[k - 4] = true;

                    percentGold += 0.15f;
                }
            }
        }
        for (int i = 1; i <= 2; i++)
        {
            if (!allTile[40 * i] && !allTile[40 * i + 1] && !allTile[40 * i - 1])
            {
                Vector3 goldPosition = postitleHolder[40 * i].position;
                Transform newItem = Instantiate(itemPrefab[0], goldPosition, Quaternion.identity) as Transform;
                newItem.parent = goldHolder;
                allTile[40 * i] = true;
            }
        }


        if (!allTile[currentTitle - 1] && !allTile[currentTitle - 2])
        {
            Vector3 goldPosition = postitleHolder[currentTitle - 1].position;
            Transform newItem = Instantiate(itemPrefab[3], goldPosition, Quaternion.identity) as Transform;
            newItem.parent = goldHolder;
            allTile[currentTitle - 1] = true;
        }


        if (!allTile[60] && !allTile[60 + 1] && !allTile[60 - 1])
        {
            Vector3 goldPosition = postitleHolder[60].position;
            if (Random.value > 0.5f)
            {
                Transform newItem = Instantiate(itemPrefab[1], goldPosition, Quaternion.identity) as Transform;
                newItem.parent = goldHolder;
                allTile[60] = true;
            }
            else
            {
                Transform newItem = Instantiate(itemPrefab[2], goldPosition, Quaternion.identity) as Transform;
                newItem.parent = goldHolder;
                allTile[60] = true;
            }
        }

    }

    public int GetPrefabTrap()
    {
        float percent = Random.value;
        if (percent < 0.3)
        {
            return 0;
        }
        else
        {
            if (percent < 0.55)
            {
                return 1;
            }
            else
            {
                if (percent < 0.75)
                {
                    return 2;
                }
                else
                {
                    if (percent < 0.9)
                    {
                        return 3;
                    }
                    else
                    {
                        if (percent < 1)
                        {

                            return Random.value > 0.5 ? 4 : 5;
                        }
                    }
                }
            }
        }
        return 0;
    }

    public Coord GetRandomTrap()
    {
        Coord randomCoord = shuffledTileTraps.Dequeue();
        shuffledTileTraps.Enqueue(randomCoord);
        return randomCoord;
    }

    public void CreateTitle()
    {
        if (transform.FindChild(titleName))
        {
            DestroyImmediate(transform.FindChild(titleName).gameObject);
        }
        Transform titleHolder = new GameObject(titleName).transform;
        titleHolder.parent = transform;


        for (int i = 0; i < currentTitle; i++)
        {
            Vector3 titelPosition = TitleToPosition(i);
            Transform newTitle = Instantiate(titlePrefab, titelPosition, Quaternion.Euler(Vector3.zero)) as Transform;
            newTitle.parent = titleHolder;
        }
    }

    Vector3 TitleToPosition(int i)
    {
        return new Vector3( posTitleStart.x + 0.4f * i, posTitleStart.y, posTitleStart.z);
    }

    public struct Coord
    {
        public int x;

        public Coord(int _x)
        {
            x = _x;
        }

    }

    public static class Utility
    {
        public static T[] ShuffleArray<T>(T[] array, ref int seed, int minVlaue,int maxValue )
        {
            
            System.Random prng = new System.Random(seed);
            int randomIndex = prng.Next(0, minVlaue);
            for (int i = 0; i < array.Length; i++)
            {
                randomIndex += minVlaue+ prng.Next(0, maxValue-minVlaue);
                if (randomIndex >= array.Length) break;
                T tempItem = array[randomIndex];
                array[randomIndex] = array[i];
                array[i] = tempItem;
            }
            seed = seed + prng.Next(0, maxValue);
            return array;
        }

    }
}
