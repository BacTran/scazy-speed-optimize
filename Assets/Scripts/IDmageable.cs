﻿using UnityEngine;

public interface IDmageable {

    void TakeHit( float pecSpeed, float timeExits, Collider2D collider);
    void TakeDamage(float damage);
}
