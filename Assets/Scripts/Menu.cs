﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public GameObject IntroductionGUI;
    public GameObject MainUI;
    public GameObject ShopUI;
    public GameObject shopCharUI;
    public GameObject shopEnviUI;

    public Sprite[] soundSprite;
    public Image imgSound;
    public Text CoinMain;



    int indexActive;

    void Start()
    {
        indexActive = PlayerPrefs.GetInt("AcSound", 0);
        imgSound.sprite = soundSprite[indexActive];
    }
    void Update()
    {
        CoinMain.text = ShopManager.instance.coinMain.ToString();
    }

    public void onSound()
    {
        indexActive = 1 - indexActive;
        AudioManager.intance.OnVolum(1 - indexActive);
        imgSound.sprite = soundSprite[indexActive];
        PlayerPrefs.SetInt("AcSound", indexActive);
        PlayerPrefs.Save();
    }

    public void onPlay()
    {

        AudioManager.intance.isPlay = true;
        SpeedManager.instance.lastTimeScore = Time.time + 3;
        SceneManager.LoadScene("CrazySpeed");
    }

    public void onIntroduction()
    {
        MainUI.SetActive(false);
        IntroductionGUI.SetActive(true);
    }
    public void backHome()
    {
        IntroductionGUI.SetActive(false);
        MainUI.SetActive(true);
    }

    public void btn_shopCharactor()
    {
        MainUI.SetActive(false);
        ShopUI.SetActive(true);
        shopCharUI.SetActive(true);
    }

    public void btn_shopEnvi()
    {
        MainUI.SetActive(false);
        ShopUI.SetActive(true);
        shopEnviUI.SetActive(true);
    }
    public void Home()
    {
        SceneManager.LoadScene("Menu");
    }

    public void btn_Chatactor()
    {
        shopEnviUI.SetActive(false);
        shopCharUI.SetActive(true);
    }

    public void btn_Envi()
    {
        shopCharUI.SetActive(false);
        shopEnviUI.SetActive(true);
    }

}
