﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(BoxCollider2D))]
public class Trap : ILiving {

    Camera isCameraRender;

    public bool isorginSpeed;
    public float orginSpeed;
    public float damage;
    public float percentSpeed;
    public float timeExits;
    float direction = -1;
    float speed;


    Animator myAnimator;
    SpriteRenderer mySpriteRenderer;
    SpeedManager speedManager;
    PlayerController playController;

    public static event System.Action onTriggerTrap;

    public override  void  Start()
    {
        myAnimator = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        isCameraRender = GameObject.FindGameObjectWithTag("CameraRenderer").GetComponent<Camera>();
        speedManager = FindObjectOfType<SpeedManager>();
        playController = FindObjectOfType<PlayerController>();

        speedManager.OnChangeSpeed += OnChangeSpeed;
        speed = speedManager.GetSpeed();
    }

    void Update () {
        // Movement
        if (!SpeedManager.instance.playisdead && !SpeedManager.instance.Pause && AudioManager.intance.isPlay)
        {
            Vector3 movement = Vector3.zero;
            if (isorginSpeed)
            {
                movement = new Vector3((speed) * direction, 0, 0);
            }
            else
            {
                movement = new Vector3((speed + orginSpeed) * direction, 0, 0);
            }
            

            movement *= Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x + movement.x, transform.localPosition.y, transform.position.z);
        }

        //Play animator
        if (RendererExtensions.IsVisibleFrom(mySpriteRenderer,isCameraRender) == true)
        {
            if (isorginSpeed)
            {
                isorginSpeed = false;
            }
            myAnimator.enabled = true;
        }

        //Destroy gameobject
        if (transform.localPosition.x < 0 && mySpriteRenderer.IsVisibleFrom(isCameraRender) == false)
        {
            Destroy(gameObject);
        }
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        
        if (!playController.isAttack && !playController.isFly)
        {
            IDmageable damageableObject = collider.GetComponent<IDmageable>();
            AudioManager.intance.PlaySound("Trap", transform.position);
            if (damageableObject != null)
            {
                damageableObject.TakeHit(percentSpeed, timeExits, collider);
            }
            if (onTriggerTrap != null)
            {
                onTriggerTrap();
            }
        }

        
        
    }

    public void OnChangeSpeed()
    {
        speed = speedManager.GetSpeed();
    }

    public override void Die()
    {
        base.Die();
        myAnimator.SetBool("isShield", true);
    }

    
}
