﻿using UnityEngine;
using System.Collections;

public class Item : MonoBehaviour {

    Camera isCameraRender;

    float direction = -1;
    float speed;

    Animator myAnimator;
    SpriteRenderer mySpriteRenderer;
    SpeedManager speedManager;

    PlayerController playerController;
    // Use this for initialization
    void Start () {
        myAnimator = GetComponent<Animator>();
        mySpriteRenderer = GetComponent<SpriteRenderer>();
        isCameraRender = GameObject.FindGameObjectWithTag("CameraRenderer").GetComponent<Camera>();
        speedManager = FindObjectOfType<SpeedManager>();

        speedManager.OnChangeSpeed += OnChangeSpeed;
        speed = speedManager.GetSpeed();

        playerController = FindObjectOfType<PlayerController>();
    }
	
	// Update is called once per frame
	void Update () {
        // Movement
        if (!SpeedManager.instance.playisdead && !SpeedManager.instance.Pause && AudioManager.intance.isPlay)
        {

        
            Vector3 movement = new Vector3(speed  * direction, 0, 0);

            movement *= Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x + movement.x, transform.localPosition.y, transform.position.z);
        }

        //Play animator
        if (RendererExtensions.IsVisibleFrom(mySpriteRenderer, isCameraRender) == true)
        {
            myAnimator.enabled = true;
        }

        //Destroy gameobject
        if (transform.localPosition.x < 0 && mySpriteRenderer.IsVisibleFrom(isCameraRender) == false)
        {
            Destroy(gameObject);
        }
    }


    public void OnChangeSpeed()
    {
        speed = speedManager.GetSpeed();

    }
}
