﻿using UnityEngine;
using System.Collections;
using System;

public class SpeedManager : MonoBehaviour {

    public float[] speedGauge;
    [HideInInspector]
    public int indexGuage;
    bool isOne;

    public float startingSpeed;

    
    public float speed;

    public event System.Action OnChangeSpeed;
    public event System.Action OnDamge;


    public static SpeedManager instance;
    [HideInInspector]
    public bool Pause;
    [HideInInspector]
    public bool Jump;

    [HideInInspector]
    public float lastTimeScore;

    bool timeTrigger;
    IEnumerator aniTimeExit;

    bool dungThoiGian;
    bool chophepvancham;
    bool vuaangiamtoc;
    IEnumerator tempFunctionItem;
    IEnumerator tempFunctionTrap;

    float tempTime;
    float speedFristItem;
    float speedFristTrap;
    float specSpeedFirst;
    float speedLast;

    PlayerController playController;

    public bool playisdead;
    void Awake()
    {

        instance = this;
        speed = startingSpeed;
    }
    void Start()
    {
        lastTimeScore = Time.time + 3;
        indexGuage = -1;


        playController = FindObjectOfType<PlayerController>();

       FindObjectOfType<PlayerController>().OnDeath += StopAllIEnumerators;
    }


    void Update()
    {
        if (Time.time > lastTimeScore)
        {
            lastTimeScore = Time.time + 5;
            if (!dungThoiGian && AudioManager.intance.isPlay)
            {

                speed += speed * 0.25f;
                if (OnChangeSpeed != null)
                {
                    OnChangeSpeed();
                }
                
            }
            
        }
        if (AudioManager.intance.isPlay)
        {
            if (kiemTraTocDo()&&GameObject.FindObjectOfType<PlayerController>())
            {
                if (indexGuage >= speedGauge.Length-1 )
                {
                    if (!isOne)
                    {
                        StartCoroutine("IEBocChay");
                        isOne = true;
                    }
                }
                else
                {
                    StopCoroutine("IEBocChay");
                    isOne = false;
                }
            }
        }
    }
    IEnumerator IEBocChay()
    {
        float percent = 0;
        while(percent < 5)
        {
            percent += Time.deltaTime;
            yield return null;
        }

        OnDamge();
    }

    bool kiemTraTocDo()
    {
        int i = 0;
        while (i < speedGauge.Length)
        {
            if (speed >= speedGauge[i] * 0.4)
            {
                indexGuage = speedGauge.Length-i;
                return true;
            }
            i++;
        }
        indexGuage = 0;
        return false;
    }

    public void GiamToc()
    {
        //Debug.Log("An thuoc giam toc!");
        dungThoiGian = false;
        vuaangiamtoc = true;
        if (tempFunctionItem != null)
        {
            //Debug.Log("tempFunctionItem co ham dang chay!");
            speed = speedFristItem;
            tempFunctionItem = null;
        }
        if (tempFunctionTrap != null)
        {
            //Debug.Log("tempFunctionTrap co ham dang chay!");

            StopCoroutine(tempFunctionTrap);
            speed = speedFristTrap;
            specSpeedFirst = 0;
            tempFunctionTrap = null;
        }

        float temp = speed / 2;
        if (temp > startingSpeed) {
            speed = temp;
        }
        else
        {
            speed = startingSpeed;
        }
        if (OnChangeSpeed != null)
        {
            OnChangeSpeed();
        }
    }

    public void thayDoitocDOkhiVaTrap(float time, float pecSpeed)
    {
        
        if (pecSpeed > specSpeedFirst || chophepvancham)
        {
            //Debug.Log("Func: thayDoitocDOkhiVaTrap");
            dungThoiGian = true;

            if (tempFunctionItem != null)
            {
                StopCoroutine(tempFunctionItem);
                speed = speedFristItem;
                tempFunctionItem = null;
            }

            if (tempFunctionTrap != null)
            {
                //Debug.Log("temptempFunction co ham dang chay!");

                StopCoroutine(tempFunctionTrap);
                speed = speedFristTrap;
                specSpeedFirst = 0;
                tempFunctionTrap = null;
            }
            specSpeedFirst = pecSpeed;
            speedFristTrap = speed;

            tempTime = speed * pecSpeed;
            speed += tempTime;

            speedLast = speed;
            if (OnChangeSpeed != null)
            {
                OnChangeSpeed();
            }
            tempFunctionTrap = khoangThoiGianChoTrap(time);
            StartCoroutine(tempFunctionTrap);
        }
        else
        {
            //Debug.Log("Trap nay co tac dung kem hon trap trc or chua toi thoi diem an them!");
        }

    }

    private IEnumerator khoangThoiGianChoTrap(float time)
    {
        chophepvancham = false;
        float percent = 0;
        while (percent < time)
        {
            percent += Time.deltaTime;

            yield return new WaitForSeconds(0);
        }

        percent = 0;
        
        while (percent<1)
        {
            specSpeedFirst = 0;
            chophepvancham = true;
            percent += Time.deltaTime/2;

            speed = Mathf.Lerp(speedLast, speedFristTrap, percent);
            if (OnChangeSpeed != null)
            {
                OnChangeSpeed();
            }
            yield return new WaitForSeconds(0);
        }

        dungThoiGian = false;
        tempFunctionTrap = null;
        speedFristTrap = 0;
        speedLast = 0;
        

        //Debug.Log("Xong thoi gian tac dung khi va cham Trap!");


    }

    public void thayDoitocDokhiAnGiant(float time)
    {

        //Debug.Log("Func: thayDoitocDokhiAnGiant");
        dungThoiGian = true;

        if (tempFunctionItem != null)
        {
            //Debug.Log("tempFunctionItem co ham dang chay!");
            StopCoroutine(tempFunctionItem);
            speed = speedFristItem;
            tempFunctionItem = null;
        }

        if (tempFunctionTrap != null)
        {
            //Debug.Log("tempFunctionTrap co ham dang chay!");

            StopCoroutine(tempFunctionTrap);
            speed = speedFristTrap;
            specSpeedFirst = 0;
            tempFunctionTrap = null;
        }
        speedFristItem = speed;
        tempTime = speed / 2;
        if (tempTime > startingSpeed)
        {
            speed = tempTime;
        }
        else
        {
            speed = startingSpeed;
        }
        
        if (OnChangeSpeed != null)
        {
            OnChangeSpeed();
        }

        tempFunctionItem = khoangThoiGianChoGiant(time);
        StartCoroutine(tempFunctionItem);
    }

    private IEnumerator khoangThoiGianChoGiant(float time)
    {
        float percent = 0;
        while (percent < time)
        {
            percent += Time.deltaTime;

            yield return new WaitForSeconds(0);
        }
        if (!vuaangiamtoc)
        {
            speed += tempTime;
        }
        else
        {
            vuaangiamtoc = false;
        }
        
        if (OnChangeSpeed != null)
        {
            OnChangeSpeed();
        }

        playController.SetAniGiant(false);
        dungThoiGian = false;
        tempFunctionItem = null;
        tempTime = 0;

        //Debug.Log("Xong thoi gian tac dung khi an Giant !");
    }

    public void thayDoitocDokhiAnBroom(float time)
    {
        //Debug.Log("Func: thayDoitocDokhiAnBroom");
        dungThoiGian = true;
        if (tempFunctionTrap != null)
        {
            StopCoroutine(tempFunctionTrap);
            speed = speedFristTrap;
            tempFunctionTrap = null;
        }
        if (tempFunctionItem != null)
        {  
            StopCoroutine(tempFunctionItem);
            speed = speedFristTrap;
            specSpeedFirst = 0;
            tempFunctionItem = null;
        }
        speedFristItem = speed;
        tempTime = speed * .75f ;
        speed += tempTime;
        if (OnChangeSpeed != null)
        {
            OnChangeSpeed();
        }

        tempFunctionItem = khoangThoiGianChoBroom(time);
        StartCoroutine(tempFunctionItem);
    }

    private IEnumerator khoangThoiGianChoBroom(float time)
    {
        float percent = 0;
        while (percent < time)
        {
            percent += Time.deltaTime;

            yield return new WaitForSeconds(0);
        }

        
        if (!vuaangiamtoc)
        {
            speed -= tempTime;
        }
        else
        {
            vuaangiamtoc = false;
        }

        if (OnChangeSpeed != null)
        {
            OnChangeSpeed();
        }

        playController.SetAniFly(false);

        dungThoiGian = false;
        tempFunctionItem = null;
        tempTime = 0;
        //Debug.Log("Xong thoi gian tac dung khi an Broom !");
    }

    public void thayDoitocDokhiAnShield(float time)
    {
       // Debug.Log("Func: thayDoitocDokhiAnShield");

        dungThoiGian = true;
        if (tempFunctionTrap != null)
        {
            StopCoroutine(tempFunctionTrap);
            speed = speedFristTrap;
            tempFunctionTrap = null;
        }
        if (tempFunctionItem != null)
        {
            StopCoroutine(tempFunctionItem);
            speed = speedFristTrap;
            specSpeedFirst = 0;
            tempFunctionItem = null;
        }
        speedFristItem = speed;
        tempFunctionItem = khoangThoiGianChoShield(time);
        StartCoroutine(tempFunctionItem);
    }

    private IEnumerator khoangThoiGianChoShield(float time)
    {
        float percent = 0;
        while (percent < time)
        {
            percent += Time.deltaTime;

            yield return new WaitForSeconds(0);
        }

        playController.SetAniShield(false);

        dungThoiGian = false;
        tempFunctionItem = null;

       // Debug.Log("Xong thoi gian tac dung khi an Shield !");
    }

    public void StopAllIEnumerators()
    {
        StopAllCoroutines();
    }
    public float GetSpeed()
    {
        return speed;
    }
}
