﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScrollRectSnapCharactor : MonoBehaviour {

    public Text coinStoreUI;
    public Text nameItemStoreUI;
    public GameObject btnStoreUI;


    public RectTransform pancel;
    public ShopItem[] img;
    public RectTransform center;

    float[] distance;
    bool dragging = false;
    int imgDistance;
    int minImageNum;

    void Start()
    {
        int imgLenght = img.Length;
        distance = new float[imgLenght];

        imgDistance = (int)Mathf.Abs(img[1].img.GetComponent<RectTransform>().anchoredPosition.x - img[0].img.GetComponent<RectTransform>().anchoredPosition.x);

        StartCoroutine("ScrollRectSnapImg");
        img[ShopManager.instance.indexActiveChara].img.GetComponent<RectTransform>().localScale = Vector3.one;

    }


    IEnumerator aniSnapImgActive()
    {
        float newX = Mathf.Lerp(pancel.anchoredPosition.x, ShopManager.instance.indexActiveChara * (-imgDistance), 1);
        Vector2 newPosition = new Vector2(newX, pancel.anchoredPosition.y);
        pancel.anchoredPosition = newPosition;
        yield return null;
    }

    IEnumerator ScrollRectSnapImg()
    {
        yield return  StartCoroutine("aniSnapImgActive");
        while (true)
        {
            for (int i = 0; i < img.Length; i++)
            {
                distance[i] = (int)Mathf.Abs(center.transform.position.x - img[i].img.transform.position.x);
            }

            float minDistance = Mathf.Min(distance);

            for (int i = 0; i < img.Length; i++)
            {
                if (distance[i] < 248)
                {
                    if (distance[i] > 0)
                    {
                        Double perLocalScale = Math.Round((1 - distance[i] / 248), 6);

                        if (perLocalScale > .5f)
                        {
                            img[i].img.GetComponent<RectTransform>().localScale = Vector3.one * (float)perLocalScale;
                        }

                    }
                }
                else
                {
                    img[i].img.GetComponent<RectTransform>().localScale = Vector3.one * .5f;
                }
            }

            for (int i = 0; i < img.Length; i++)
            {
                if (minDistance == distance[i])
                {
                    minImageNum = i;
                    if (ShopManager.instance.characActive[minImageNum] ==0)
                    {

                        btnStoreUI.SetActive(true);
                        coinStoreUI.text = img[i].coin.ToString();
                    }
                    else
                    {
                        ShopManager.instance.SetIndexCharac(minImageNum);
                        ShopManager.instance.SetNameCharacter(img[minImageNum].id);
                        btnStoreUI.SetActive(false);
                    }

                    nameItemStoreUI.text = img[i].id;
                }
            }

            if (!dragging)
            {
                LerpToImg(minImageNum * -imgDistance);
            }
            yield return null;
        }
    }

    private void LerpToImg(int pos)
    {
        float newX = Mathf.Lerp(pancel.anchoredPosition.x, pos, Time.deltaTime * 5f);
        Vector2 newPosition = new Vector2(newX, pancel.anchoredPosition.y);
        pancel.anchoredPosition = newPosition;


    }

    public void StartDrag()
    {
        dragging = true;
    }
    public void EndDrag()
    {
        dragging = false;
    }

    void OnDisable()
    {
        StopCoroutine("ScrollRectSnapImg");
        float newX = Mathf.Lerp(pancel.anchoredPosition.x, ShopManager.instance.indexActiveChara * (-imgDistance), 1);
        Vector2 newPosition = new Vector2(newX, pancel.anchoredPosition.y);
        pancel.anchoredPosition = newPosition;
        btnStoreUI.SetActive(false);
    }
    void OnEnable()
    {
        img[ShopManager.instance.indexActiveChara].img.GetComponent<RectTransform>().localScale = Vector3.one;

        StartCoroutine("ScrollRectSnapImg");
    }

    public void btn_Store()
    {
        if (ShopManager.instance.coinMain > img[minImageNum].coin)
        {
            ShopManager.instance.coinMain -= img[minImageNum].coin;
            PlayerPrefs.SetInt("CoinMain", ShopManager.instance.coinMain);
            PlayerPrefs.Save();
            img[minImageNum].img.GetComponent<Animator>().SetTrigger("ShopDone");

            img[minImageNum].coin = 0;
            ShopManager.instance.SetIndexCharac(minImageNum);
            ShopManager.instance.SetNameCharacter(img[minImageNum].id);
        }
    }


    [Serializable]
    public class ShopItem
    {
        public string id;
        public Image img;
        public int coin;

    }

}
