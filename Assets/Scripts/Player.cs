﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(PlayerController))]
public class Player : MonoBehaviour
{

    PlayerController controller;

    public float jumpVelocity = 0.5f;
    public LayerMask itemLayerMask;

    Vector2 pointA;
    Vector2 pointB;

    void Start()
    {
        controller = GetComponent<PlayerController>();
        

    }

    void Update()
    {
        if (controller != null&& AudioManager.intance.isPlay)
        {
            if (Input.GetMouseButtonDown(0))
            {
                controller.Jump(jumpVelocity);
            }
        }
        pointA = GetComponent<BoxCollider2D>().bounds.min;
        pointB = GetComponent<BoxCollider2D>().bounds.max;
        Collider2D collider2D = Physics2D.OverlapArea(pointA, pointB, itemLayerMask);
        if (collider2D!=null)
        {
            controller.TakeItem(collider2D);
        }
    }

}
