﻿using UnityEngine;
using System.Collections;

public class ScoreKeeper : MonoBehaviour {

    
    public static int score {
        get; set; }
    
    public static int gold { get; set; }

    
    public float lastTimeScore;
    // Use this for initialization
    void Start () {
        Trap.onTriggerTrap += OnTriggerTrap;
        PlayerController.OnTriggerStaticGold += OnTriggerGold;
        lastTimeScore = Time.time + 1;
        score = 0;
        gold = 0;
        
    }

    void Update()
    {
        if (Time.time > lastTimeScore && AudioManager.intance.isPlay)
        {
            lastTimeScore = Time.time + 1;
            if (FindObjectOfType<Player>() != null && !SpeedManager.instance.Pause)
            {
                score += 3;
            }
        }
    }

    void OnTriggerTrap()
    {
        score -= 5;
    }
    void OnTriggerGold()
    {
        gold ++;
        
       
    }

}
