﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class ScrollingScript : MonoBehaviour
{


    float speed;
    public float tocdorieng;
    public float wrongNumberPath;

    float direction = -1;
    private List<Transform> backgroundPart;
    SpeedManager speedManager;

    void Start()
    {

        backgroundPart = new List<Transform>();

        for (int i = 0; i < transform.childCount; i++)
        {
            Transform child = transform.GetChild(i);

            if (child.GetComponent<Renderer>() != null )
            {
                backgroundPart.Add(child);
            }
        }
        backgroundPart = backgroundPart.OrderBy(
          t => t.position.x
        ).ToList();


        speedManager = FindObjectOfType<SpeedManager>();

        speedManager.OnChangeSpeed += OnChangeSpeed;
        speed = speedManager.GetSpeed();


    }

    void Update()
    {
        // Movement

        if (!SpeedManager.instance.playisdead && !SpeedManager.instance.Pause)
        {
            Vector3 movement = new Vector3((speed+ tocdorieng) * direction, 0, 0);

            movement *= Time.deltaTime;
            transform.localPosition = new Vector3(transform.localPosition.x + movement.x, transform.localPosition.y, transform.localPosition.z);
        }

        Transform firstChild = backgroundPart.FirstOrDefault();

        if (firstChild != null)
        {
            if (firstChild.GetComponent<SpriteRenderer>().IsVisibleFrom(Camera.main) == false)
            {
                Transform lastChild = backgroundPart.LastOrDefault();
                Vector3 lastPosition = lastChild.transform.localPosition;
                Vector3 lastSize = (lastChild.GetComponent<SpriteRenderer>().bounds.max - lastChild.GetComponent<SpriteRenderer>().bounds.min);
                //print(lastSize.x);
                firstChild.localPosition = new Vector3(lastPosition.x +lastSize.x - wrongNumberPath, firstChild.localPosition.y, firstChild.localPosition.z);

                backgroundPart.Remove(firstChild);
                backgroundPart.Add(firstChild);
            }
        }

    }
    public void OnChangeSpeed()
    {

        speed = speedManager.GetSpeed();
    }

}
